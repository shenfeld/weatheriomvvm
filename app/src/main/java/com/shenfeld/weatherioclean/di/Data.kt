package com.shenfeld.weatherioclean.di

import com.shenfeld.weatherioclean.Constants
import com.shenfeld.weatherioclean.data.WeatherApi
import com.shenfeld.weatherioclean.data.repositories.DayRepository
import com.shenfeld.weatherioclean.domain.repositories.DayDataSource
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val dataModule = module {
    single<WeatherApi> {
        Retrofit.Builder()
            .baseUrl(Constants.API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(WeatherApi::class.java)
    }
    single<DayDataSource> {
        DayRepository(weatherApi = get())
    }
}

