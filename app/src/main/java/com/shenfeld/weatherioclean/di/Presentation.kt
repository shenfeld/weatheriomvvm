package com.shenfeld.weatherioclean.di

import com.shenfeld.weatherioclean.presentation.MainViewModel
import com.shenfeld.weatherioclean.presentation.utils.ResourcesManager
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    viewModel { MainViewModel() }

    single { ResourcesManager(androidContext()) }

}