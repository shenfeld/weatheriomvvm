package com.shenfeld.weatherioclean.di

import com.shenfeld.weatherioclean.domain.repositories.DayDataSource
import com.shenfeld.weatherioclean.domain.usecases.GetWeatherInfoUseCase
import com.shenfeld.weatherioclean.domain.usecases_impl.GetWeatherInfoUseCaseImpl
import org.koin.dsl.module

val domainModule = module {
    factory<GetWeatherInfoUseCase> {
        GetWeatherInfoUseCaseImpl(dayDataSource = get())
    }
}