package com.shenfeld.weatherioclean.domain.usecases_impl

import com.shenfeld.weatherio.utils.Cities
import com.shenfeld.weatherioclean.domain.models.Day
import com.shenfeld.weatherioclean.domain.repositories.DayDataSource
import com.shenfeld.weatherioclean.domain.usecases.GetWeatherInfoUseCase

class GetWeatherInfoUseCaseImpl(
    private val dayDataSource: DayDataSource
) : GetWeatherInfoUseCase {
    override suspend fun execute(city: Cities): List<Day> {
        return dayDataSource.getWeather(city = city)
    }
}