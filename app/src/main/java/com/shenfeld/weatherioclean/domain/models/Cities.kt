package com.shenfeld.weatherio.utils

enum class Cities(val lat: Double, val lon: Double) {
    MOSCOW(55.753960, 37.620393),
    ROSTOV_ON_DON(47.24, 39.71),
    KRASNODAR(45.040216, 38.975996),
    TAGANROG(47.209580, 38.935194)
}