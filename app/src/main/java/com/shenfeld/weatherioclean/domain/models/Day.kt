package com.shenfeld.weatherioclean.domain.models

data class Day(
    val mainWeather: String,
    val tempDay: Int,
    val timeStamp: Long,
    val humidity: Int,
    val sunset: Long,
    val sunrise: Long
)