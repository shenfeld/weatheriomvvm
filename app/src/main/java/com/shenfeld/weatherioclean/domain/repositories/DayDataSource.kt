package com.shenfeld.weatherioclean.domain.repositories

import com.shenfeld.weatherio.utils.Cities
import com.shenfeld.weatherioclean.domain.models.Day

interface DayDataSource {
    suspend fun getWeather(city: Cities): List<Day>
}