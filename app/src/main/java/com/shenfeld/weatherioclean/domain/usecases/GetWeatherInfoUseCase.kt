package com.shenfeld.weatherioclean.domain.usecases

import com.shenfeld.weatherio.utils.Cities
import com.shenfeld.weatherioclean.domain.models.Day

interface GetWeatherInfoUseCase {
    suspend fun execute(city: Cities): List<Day>
}