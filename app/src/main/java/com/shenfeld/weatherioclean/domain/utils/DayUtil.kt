package com.shenfeld.weatherioclean.domain.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object DayUtil {
    const val DATE_FORMAT = "EEEE, d MMM"
    const val DAY_OF_WEEK = "EEEE"
    const val DATE_OF_WEEK = "d MMMM, yyyy"

    @SuppressLint("SimpleDateFormat")
    fun getSimpleDataForm(dataUnix: Long, dateFormat: String = DATE_FORMAT): String =
        SimpleDateFormat(dateFormat).format(Date(dataUnix))
}

