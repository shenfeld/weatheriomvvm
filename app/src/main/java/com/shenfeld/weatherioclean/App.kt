package com.shenfeld.weatherioclean

import android.app.Application
import com.shenfeld.weatherioclean.di.dataModule
import com.shenfeld.weatherioclean.di.domainModule
import com.shenfeld.weatherioclean.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(dataModule, domainModule, presentationModule)
        }
    }
}