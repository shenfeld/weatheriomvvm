package com.shenfeld.weatherioclean.presentation

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.shenfeld.weatherio.utils.Cities
import com.shenfeld.weatherioclean.R
import com.shenfeld.weatherioclean.presentation.adapter.WeatherAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModel()
    private var adapter: WeatherAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()

        setupObservers()
        viewModel.onViewCreated()
    }

    private fun setupView() {
        adapter = WeatherAdapter()
        days_recycler_view.adapter = adapter
        days_recycler_view.layoutManager = LinearLayoutManager(this)
    }

    private fun setupObservers() {
        with(viewModel) {
            titleLiveData.observe(this@MainActivity) {
                tvNameCity.text = it
            }


            dayModelLiveData.observe(this@MainActivity) {
                adapter?.updateAllItems(it)
            }

            dayStateLiveData.observe(this@MainActivity) {
                when (it) {
                    DayState.DAY -> ivCurrentTimesOfFay.setImageResource(R.drawable.day)
                    DayState.NIGHT -> ivCurrentTimesOfFay.setImageResource(R.drawable.night)
                }
            }

            humidityLiveData.observe(this@MainActivity) {
                tvCurrentDate.text = it
            }

            descriptionLiveData.observe(this@MainActivity) {
                tvCurrentMainWeather.text = it
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val selectedCity = when (item.itemId) {
            R.id.moscow -> {
                Cities.MOSCOW
            }
            R.id.rostov -> {
                Cities.ROSTOV_ON_DON
            }
            R.id.krasnodar -> {
                Cities.KRASNODAR
            }
            R.id.taganrog -> {
                Cities.TAGANROG
            }
            else -> null
        }
        selectedCity?.let {
            viewModel.onCitySelected(it)
        }
        return super.onOptionsItemSelected(item)
    }

    enum class DayState() {
        DAY, NIGHT
    }
}