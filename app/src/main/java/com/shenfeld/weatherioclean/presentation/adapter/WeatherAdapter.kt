package com.shenfeld.weatherioclean.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shenfeld.weatherioclean.R
import com.shenfeld.weatherioclean.presentation.base.BaseAdapter

class WeatherAdapter : BaseAdapter<DayModel, WeatherAdapter.WeatherViewHolder>() {
    class WeatherViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        internal var dayOfWeek = v.findViewById<TextView>(R.id.tv_day_of_week)
        internal var dateOfWeek = v.findViewById<TextView>(R.id.tv_date_of_week)
        internal var dayTemp = v.findViewById<TextView>(R.id.tv_day_temperature)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WeatherViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_day, parent,
                false
            )
        return WeatherViewHolder(view)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        val current = items[position]
        holder.dayOfWeek.text = current.dayOfWeek
        holder.dateOfWeek.text = current.dateOfWeek
        holder.dayTemp.text = "${current.dayTemp}°"
    }
}