package com.shenfeld.weatherioclean.presentation.adapter

data class DayModel(
    val dayOfWeek: String,
    val dateOfWeek: String,
    val dayTemp: Int
)