package com.shenfeld.weatherioclean.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shenfeld.weatherio.utils.Cities
import com.shenfeld.weatherioclean.R
import com.shenfeld.weatherioclean.domain.models.Day
import com.shenfeld.weatherioclean.domain.usecases.GetWeatherInfoUseCase
import com.shenfeld.weatherioclean.domain.utils.DayUtil
import com.shenfeld.weatherioclean.presentation.MainActivity.DayState
import com.shenfeld.weatherioclean.presentation.adapter.DayModel
import com.shenfeld.weatherioclean.presentation.utils.ResourcesManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

class MainViewModel : ViewModel(), KoinComponent {
    private val resourceManager: ResourcesManager by inject()
    private val getWeatherInfoUseCase: GetWeatherInfoUseCase by inject()

    val titleLiveData = MutableLiveData<String>()
    val dayStateLiveData = MutableLiveData<DayState>()
    val humidityLiveData = MutableLiveData<String>()
    val descriptionLiveData = MutableLiveData<String>()
    val dayModelLiveData = MutableLiveData<List<DayModel>>()

    fun onViewCreated() {
        onCitySelected(Cities.ROSTOV_ON_DON)
    }

    fun onCitySelected(city: Cities) {
        val cityStringId = when (city) {
            Cities.MOSCOW -> R.string.moscow
            Cities.ROSTOV_ON_DON -> R.string.rostov_on_don
            Cities.KRASNODAR -> R.string.krasnodar
            Cities.TAGANROG -> R.string.taganrog
        }
        val cityName = resourceManager.getString(cityStringId)
        titleLiveData.postValue(cityName)
        getWeather(city)
    }

    private fun getWeather(city: Cities) {

        viewModelScope.launch(Dispatchers.IO) {
            val days = getWeatherInfoUseCase.execute(city)
            val dayModels = days.map {
                DayModel(
                    dayOfWeek = DayUtil.getSimpleDataForm(
                        dataUnix = it.timeStamp,
                        dateFormat = DayUtil.DAY_OF_WEEK
                    ),
                    dateOfWeek = DayUtil.getSimpleDataForm(
                        dataUnix = it.timeStamp,
                        dateFormat = DayUtil.DATE_OF_WEEK
                    ),
                    dayTemp = it.tempDay
                )
            }

            val currentDay = days[0]
            dayModelLiveData.postValue(dayModels)
            descriptionLiveData.postValue(
                resourceManager.getString(
                    R.string.description,
                    currentDay.mainWeather,
                    currentDay.tempDay
                )
            )
            humidityLiveData.postValue(
                resourceManager.getString(
                    R.string.humidity,
                    currentDay.humidity
                )
            )
            dayStateLiveData.postValue(getDayState(day = currentDay))

        }
    }

    private fun getDayState(day: Day): DayState {
        val sunset = day.sunset
        val sunrise = day.sunrise
        val currentTime = Date().time
        return if (currentTime in sunrise until sunset) {
            DayState.DAY
        } else {
            DayState.NIGHT
        }
    }

}