package com.shenfeld.weatherio.data

import com.google.gson.annotations.SerializedName

data class CityResponse(
    @SerializedName("daily") val days: List<DayResponse>
) {
    class DayResponse(
        @SerializedName("dt") val dt: Long,
        @SerializedName("sunrise") val sunrise: Long,
        @SerializedName("sunset") val sunset: Long,
        @SerializedName("temp") val temp: TempResponse,
        @SerializedName("weather") val weather: List<WeatherResponse>,
        @SerializedName("humidity") val humidity: Int
    )

    class TempResponse(
        @SerializedName("day") val day: Double,
        @SerializedName("min") val min: Double,
        @SerializedName("max") val max: Double,
        @SerializedName("night") val night: Double,
        @SerializedName("eve") val eve: Double,
        @SerializedName("morn") val morn: Double
    )

    class WeatherResponse(
        @SerializedName("id") val id: Int,
        @SerializedName("main") val main: String,
        @SerializedName("description") val description: String,
        @SerializedName("icon") val icon: String
    )
}