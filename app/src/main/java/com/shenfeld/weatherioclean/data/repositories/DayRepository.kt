package com.shenfeld.weatherioclean.data.repositories

import com.shenfeld.weatherio.utils.Cities
import com.shenfeld.weatherioclean.Constants
import com.shenfeld.weatherioclean.data.WeatherApi
import com.shenfeld.weatherioclean.domain.models.Day
import com.shenfeld.weatherioclean.domain.repositories.DayDataSource
import com.shenfeld.weatherioclean.domain.utils.DayUtil
import kotlin.math.roundToInt

class DayRepository(
    private val weatherApi: WeatherApi
) : DayDataSource {
    override suspend fun getWeather(city: Cities): List<Day> {
        val days = weatherApi.getWeather(city.lat, city.lon, Constants.API_KEY).days
        return days.map {
            Day(
                mainWeather = it.weather[0].main,
                tempDay = it.temp.day.roundToInt(),
                timeStamp = it.dt * 1000,
                humidity = it.humidity,
                sunset = it.sunset * 1000,
                sunrise = it.sunrise * 1000
            )
        }
    }
}